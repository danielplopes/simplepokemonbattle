//
//  PokemonClass.swift
//  SimplePokemonBattle
//

import Foundation
import UIKit

public class Pokemon: Identifiable { //Classe do Pokemon e a suas variáveis
    
    public var id = UUID()
    var isActive = true
    var name:String
    var image:UIImage
    var baseHp:Int
    var hp:Int
    var baseAttack:Int
    var attack:Int
    var baseDefense:Int
    var defense:Int
    var type:String
    var strongAg:String
    var weakAg:String
    var level = 1
    var maxLevel = 100
    var xpCurrent = 0
    var xpNext:Int
    
    init(aName:String, aImage:String, aType:String, aBaseHP:Int, aBaseAttack:Int, aBaseDefense:Int){ //Inicialização das variáveis
        self.name = aName
        self.image = UIImage(named: aImage) ?? UIImage.defImg
        self.type = aType
        
        self.baseHp = aBaseHP
        self.baseAttack = aBaseAttack
        self.baseDefense = aBaseDefense
        
        self.hp = Int(floor(0.01 * Double(2 * self.baseHp * self.level))) + self.level + 10 //Formula adaptada do jogo real
        self.attack = Int(floor(0.01 * Double((2 * self.baseAttack )) * Double(self.level))) + 5 //Formula adaptada do jogo real
        self.defense = Int(floor(0.01 * Double((2 * self.baseDefense )) * Double(self.level))) + 5 //Formula adaptada do jogo real
        
        self.xpNext = Int(floor(pow((Double(self.level) / 0.15), 1.75))) //Formula adaptada do jogo real
        
        switch aType { //Definição do Tipo Forte e Fraco com base no Tipo indicado pelo User
        case "Fire":
            self.strongAg = "Grass"
            self.weakAg = "Water"
        case "Water":
            self.strongAg = "Fire"
            self.weakAg = "Grass"
        case "Grass":
            self.strongAg = "Water"
            self.weakAg = "Fire"
        case "Electric":
            self.strongAg = "Flying"
            self.weakAg = "Grass"
        case "Psychic":
            self.strongAg = "Fighting"
            self.weakAg = "Bug"
        case "Bug":
            self.strongAg = "Psychic"
            self.weakAg = "Fighting"
        case "Fighting":
            self.strongAg = "Bug"
            self.weakAg = "Psychic"
        case "Flying":
            self.strongAg = "Grass"
            self.weakAg = "Electric"
        default:
            self.strongAg = "Fire"
            self.weakAg = "Grass"
        }
    }
    
    func Edited (aName:String, aImage:String, aType:String, aBaseHP:Int, aBaseAttack:Int, aBaseDefense:Int){ //Função para quando são editados os detalhes do Pokemon
        self.name = aName
        self.image = UIImage(named: aImage) ?? UIImage.defImg
        self.type = aType
        
        self.baseHp = aBaseHP
        self.baseAttack = aBaseAttack
        self.baseDefense = aBaseDefense
        
        self.hp = Int(floor(0.01 * Double(2 * self.baseHp * self.level))) + self.level + 10
        self.attack = Int(floor(0.01 * Double((2 * self.baseAttack )) * Double(self.level))) + 5
        self.defense = Int(floor(0.01 * Double((2 * self.baseDefense )) * Double(self.level))) + 5
        
        self.xpNext = Int(floor(pow((Double(self.level) / 0.15), 1.75)))
        
        switch aType {
        case "Fire":
            self.strongAg = "Grass"
            self.weakAg = "Water"
        case "Water":
            self.strongAg = "Fire"
            self.weakAg = "Grass"
        case "Grass":
            self.strongAg = "Water"
            self.weakAg = "Fire"
        case "Electric":
            self.strongAg = "Flying"
            self.weakAg = "Grass"
        case "Psychic":
            self.strongAg = "Fighting"
            self.weakAg = "Bug"
        case "Bug":
            self.strongAg = "Psychic"
            self.weakAg = "Fighting"
        case "Fighting":
            self.strongAg = "Bug"
            self.weakAg = "Psychic"
        case "Flying":
            self.strongAg = "Grass"
            self.weakAg = "Electric"
        default:
            self.strongAg = "Fire"
            self.weakAg = "Grass"
        }
    }
    
    func isStrongAg (aTypeAdvPoke:String) -> Bool { //Função valida se é Forte contra Tipo Adv
        return  aTypeAdvPoke == self.strongAg
    }
   
    func isWeakAg (aTypeAdvPoke:String) -> Bool { //Função valida se é Fraco contra Tipo Adv
        return  aTypeAdvPoke == self.weakAg
    }

    func afterBattle (aIsWinner:Bool){ //Função após batalha para adicionar XP
        if aIsWinner{
            self.xpCurrent += 50
        } else {
            self.xpCurrent += 25
        }

        lvlUp()
    }
    
    func lvlUp (){ //Função para validar se faz Level Up e sim atualiza variaveis
        if self.level != self.maxLevel {
            if self.xpCurrent >= self.xpNext{
                self.level += 1
                
                self.hp = Int(floor(0.01 * Double(2 * self.baseHp * self.level))) + self.level + 10
                self.attack = Int(floor(0.01 * Double((2 * self.baseAttack )) * Double(self.level))) + 5
                self.defense = Int(floor(0.01 * Double((2 * self.baseDefense )) * Double(self.level))) + 5
                
                self.xpNext = Int(floor(pow((Double(self.level) / 0.15), 1.75)))
            }
        }
    }
}


public class Pokedex{ // Adaptação para ter um Array Global
    static let shared = Pokedex()
    var pokedexAr = [Pokemon]()
    
    func getArray()->[Pokemon]{ //Get do conteudo do Array
        return pokedexAr
    }
    
    func startArray(){ //Funçao para Inicializaçao do Array
        pokedexAr.append(Pokemon(aName: "Bulbasaur", aImage: "bulbasaur", aType: "Grass", aBaseHP: 45, aBaseAttack: 49, aBaseDefense: 49))
        pokedexAr.append(Pokemon(aName: "Charmander", aImage: "charmander", aType: "Fire", aBaseHP: 39, aBaseAttack: 52, aBaseDefense: 43))
        pokedexAr.append(Pokemon(aName: "Squirtle", aImage: "squirtle", aType: "Water", aBaseHP: 44, aBaseAttack: 48, aBaseDefense: 65))
        pokedexAr.append(Pokemon(aName: "Pikachu", aImage: "pikachu", aType: "Electric", aBaseHP: 35, aBaseAttack: 55, aBaseDefense: 40))
        pokedexAr.append(Pokemon(aName: "Mew", aImage: "mew", aType: "Psychic", aBaseHP: 100, aBaseAttack: 100, aBaseDefense: 100))
        pokedexAr.append(Pokemon(aName: "Caterpie", aImage: "caterpie", aType: "Bug", aBaseHP: 45, aBaseAttack: 30, aBaseDefense: 35))
        pokedexAr.append(Pokemon(aName: "Machop", aImage: "machop", aType: "Fighting", aBaseHP: 70, aBaseAttack: 80, aBaseDefense: 50))
        pokedexAr.append(Pokemon(aName: "Pidgey", aImage: "pidgey", aType: "Flying", aBaseHP: 40, aBaseAttack: 45, aBaseDefense: 40))
    }
    
    func addPokemon(PokemonNovo: Pokemon){ //Funçao para adicionar novos Pokemons
        pokedexAr.append(PokemonNovo)
    }
}
