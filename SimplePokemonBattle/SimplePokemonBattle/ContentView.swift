//
//  ContentView.swift
//  SimplePokemonBattle
//

import SwiftUI

struct ContentView: View {
    @State var selection: String? = nil
    
    @EnvironmentObject var appState: AppState //Variavel para ajudar a voltar a Root View
    
    var body: some View {

        NavigationView { //Ecrã Principal | Root
            ZStack{
                Color.green
                    .ignoresSafeArea()
                Circle()
                    .scale(1.9)
                    .foregroundColor(.white.opacity(0.3))
                Circle()
                    .scale(1.35)
                    .foregroundColor(.white.opacity(0.7))
                               
                
                VStack{
                    Text("Simple Pokémon Battle")
                        .fontWeight(.bold)
                        .font(.custom("Verdana", size: 22))
                    
                    Button {
                        self.selection = "PokedexView"
                    } label: {
                        Text("Ver Pokédex!")
                            .frame(width: 300, height: 75, alignment: .center)
                            .background(Color.red)
                            .foregroundColor(.black)
                            .padding(.bottom, 40)
                            .cornerRadius(50)
                    }
                    
                    Button {
                        self.selection = "BattleHistoryView"
                    } label: {
                        Text("| Histórico Batalhas |")
                            .frame(width: 300, height: 50, alignment: .center)
                            .background(Color.gray)
                            .foregroundColor(.white)
                    }
                    
                    Button {
                        self.selection = "BattleView"
                    } label: {
                        Text("Batalhar!")
                            .frame(width: 300, height: 75, alignment: .center)
                            .background(Color.white)
                            .foregroundColor(.black)
                            .padding(.top, 40)
                            .cornerRadius(50)
                    }
                    Text("\n   Trabalho feito por:\nAndré Barros - 30348\n  Daniel Lopes-30351")
                        .font(.system(size:12))
                    
                }
                NavigationLink("",destination: PokedexView(tipoView: 1), tag: "PokedexView", selection: $selection) //Dependendo do botão selecionado envia para a View específica
                    .id(appState.rootViewId)
                NavigationLink("", destination: BattleHistoryView(), tag: "BattleHistoryView", selection: $selection)
                NavigationLink("", destination: PokedexView(tipoView: 2), tag: "BattleView", selection: $selection)
            }
        }
        .onAppear{Pokedex.shared.startArray()}
    }
}

struct BattleHistoryView: View {
  
    @State var searchText = ""
    @State var historyArView:[Battle] = History.shared.getArray()
    @State var pokedexArView:[Pokemon] = Pokedex.shared.getArray()
    @State var myPokemon:Pokemon = Pokemon(aName: "", aImage: "", aType: "", aBaseHP: 1, aBaseAttack: 1, aBaseDefense: 1)
    @State var advPokemon:Pokemon = Pokemon(aName: "", aImage: "", aType: "", aBaseHP: 1, aBaseAttack: 1, aBaseDefense: 1)
    
    var body: some View{
        VStack{
            List{
                if historyArView.isEmpty{
                    Text("Ainda não houve batlhas.")
                }
                else{
                    ForEach(historyArView) {
                    entry in
                        HStack{
                            if entry.winner == 1{
                                Text("Win - \(entry.pokemon1Name)   |   \(entry.pokemon2Name) - Lose")
                            } else {
                                Text("Lose - \(entry.pokemon1Name)   |   \(entry.pokemon2Name) - Win")
                            }
                        }
                    }
                }
            }
        }
    }
}

struct PokedexView: View {
    var tipoView:Int
    
    @State var searchText = ""
    @State var pokedexArView:[Pokemon] = Pokedex.shared.getArray() //Passagem do Array Global para local de forma ser mais facil manipular
    @State var cnt = 0
    
    var body: some View{
        VStack{
            List { //Efetuado a pesquisa na Pokedex e listagem sendo possivel pesquisar por Nome
                ForEach(searchText == "" ? pokedexArView: pokedexArView.filter( {$0.name.lowercased().contains(searchText.lowercased())} )) {
                entry in
                    if entry.isActive {
                        HStack {
                            Image(uiImage: entry.image)
                                    .resizable()
                                    .scaledToFill()
                                    .frame(width: 60, height: 60)
                            
                            if tipoView == 1 { //Reencaminhamento para as seguintes Views
                                NavigationLink("\(entry.name)".capitalized ,destination: DetailsView(detPokeId: entry.id))
                            }
                            
                            if tipoView == 2 && cnt > 1{
                                NavigationLink("\(entry.name)".capitalized ,destination: BattleView(myPokeId: entry.id))
                            }
                            if tipoView == 2 && cnt < 2{
                                Text("\(entry.name.capitalized)")
                            }
                        }
                        .onAppear{
                            for novo in pokedexArView{
                                if novo.isActive == true{
                                    cnt += 1
                                }
                            }
                        }
                    }
                }
            }
        }
        .onAppear{pokedexArView = Pokedex.shared.getArray()} //Necessário aqui para quando há refrescamentos da Listagem
        .searchable(text: $searchText)
        .navigationTitle("Pokédex")
        .navigationBarItems(trailing: NavigationLink("Criar Novo" ,destination: EditCreatePokeView(idPokeEdit: UUID(), opt: 2) )) //Botão para View de Criação de Novos Pokemons
    }
}

struct DetailsView: View {
    var detPokeId:UUID //Variavel para guardar Id do Pokemon previamente escolhido
    
    @State var searchText = ""
    @State var pokedexArView:[Pokemon] = Pokedex.shared.getArray()
    @State var mostraAlerta = false
    
    @State var detPokemon:Pokemon = Pokemon(aName: "", aImage: "", aType: "", aBaseHP: 1, aBaseAttack: 1, aBaseDefense: 1)
    
    @EnvironmentObject var appState: AppState
    
    var body: some View{ //Listagem de todos os detalhes do Pokemon previamente escolhido
        ZStack{
            Color.indigo
                .ignoresSafeArea()
            VStack{
                VStack{
                    Image(uiImage: detPokemon.image)
                        .resizable()
                        .scaledToFill()
                        .frame(width: 200, height: 200)
                    Text("\nNome: \(detPokemon.name)\n")
                    Text("\nVida: \(detPokemon.hp)")
                    Text("Ataque: \(detPokemon.attack)")
                    Text("Defesa: \(detPokemon.defense)\n")
                    Text("Tipo: \(detPokemon.type)")
                    Text("Forte contra: \(detPokemon.strongAg)")
                    Text("Fraco contra: \(detPokemon.weakAg)\n")
                    Text("Nível: \(detPokemon.level)")
                    Text("XP Atual: \(detPokemon.xpCurrent)\n")
                }
                .navigationBarItems(trailing: Button { //Alerta a ser mostrado ao Eliminar Pokemon
                    mostraAlerta.toggle()
                } label: {
                    Label("Eliminar Pokemon", systemImage: "trash.fill")
                    
                })
                .alert(isPresented: $mostraAlerta, content: {
                    Alert(
                        title: Text ("Tem a certeza?"),
                        message: Text ("Tem a certeza?"),
                        primaryButton: .destructive(Text("Eliminar"), action: {
                            detPokemon.isActive = false
                            appState.rootViewId = UUID()
                        }),
                        secondaryButton: .cancel())
                })
                NavigationLink("| Editar |\n" ,destination: EditCreatePokeView(idPokeEdit: self.detPokeId ,opt: 1) ) //Botão para ir Editar os Detalhes do Pokemon
            }
            .onAppear{
                for entry in pokedexArView{
                    if entry.id == self.detPokeId {
                        detPokemon = entry
                    }
                }
            }
            .background(Color.white)
            .padding(10)
            .overlay(RoundedRectangle(cornerRadius: 16) .strokeBorder(lineWidth: 10))
        }
    }
}

struct EditCreatePokeView: View {
    var idPokeEdit:UUID
    var opt:Int
    
    @State var searchText = ""
    @State var pokedexArView:[Pokemon] = Pokedex.shared.getArray()
    
    @EnvironmentObject var appState: AppState
    
    @State var name = "" //Variaveis para os campos de ediçao/criação dos Pokemons com alguns valores previamente definidos
    @State var baseHp = 50
    @State var baseAttack = 50
    @State var baseDefense = 50
    @State var tipo = "Fire"
    
    var body: some View{
        VStack{
            if opt == 1 { //Opção 1 - Edição do Pokemon mostando os antigos valores
                ForEach(searchText == "" ? pokedexArView: pokedexArView.filter( {$0.name.contains(searchText.lowercased())} )) {
                entry in
                    if entry.id == self.idPokeEdit {
                        Form{
                            Section{
                                TextField("Nome Atual - \(entry.name)", text: $name)
                                Stepper(value: $baseHp, in: 1...100) {
                                    Text("Vida Base Atual - \(self.baseHp)")}
                                Stepper(value: $baseAttack, in: 1...100) {
                                    Text("Ataque Base Atual - \(self.baseAttack)")}
                                Stepper(value: $baseDefense, in: 1...100) {
                                    Text("Defesa Base Atual - \(self.baseDefense)")}
                                Picker(selection: $tipo, label: Text("Tipo")){
                                    Text("Fire").tag("Fire")
                                    Text("Water").tag("Water")
                                    Text("Grass").tag("Grass")
                                    Text("Electric").tag("Electric")
                                    Text("Pyschic").tag("Psychic")
                                    Text("Bug").tag("Bug")
                                    Text("Fighting").tag("Fighting")
                                    Text("Flying").tag("Flying")
                                }
                            }
                        }
                        .onAppear{
                            self.name = entry.name
                            self.tipo = entry.type
                            self.baseHp = entry.baseHp
                            self.baseAttack  = entry.baseAttack
                            self.baseDefense = entry.baseDefense
                        }
                        .navigationBarItems(trailing: Button ("Guardar") {
                            if self.name.isEmpty{ self.name = "Ditto" }
                            
                            entry.Edited(aName: self.name.capitalized, aImage: self.name.lowercased(), aType: self.tipo, aBaseHP: self.baseHp, aBaseAttack: self.baseAttack, aBaseDefense: self.baseDefense)

                            appState.rootViewId = UUID()
                        })
                    }
                }
            }
            if opt == 2{ //Opção 2 - Criaçao do Novo Pokemon
                Form{
                    Section{
                        TextField("Nome", text: $name)
                        Stepper(value: $baseHp, in: 1...100) {
                            Text("Vida Base Atual - \(self.baseHp)")}
                        Stepper(value: $baseAttack, in: 1...100) {
                            Text("Ataque Base Atual - \(self.baseAttack)")}
                        Stepper(value: $baseDefense, in: 1...100) {
                            Text("Defesa Base Atual - \(self.baseDefense)")}
                        
                        Picker(selection: $tipo, label: Text("Tipo")){
                            Text("Fire").tag("Fire")
                            Text("Water").tag("Water")
                            Text("Grass").tag("Grass")
                            Text("Eletric").tag("Electric")
                            Text("Pyschic").tag("Psychic")
                            Text("Bug").tag("Bug")
                            Text("Fighting").tag("Fighting")
                            Text("Flying").tag("Flying")
                        }
                    }
                }
                .navigationBarItems(trailing: Button ("Criar") {
                    if self.name.isEmpty{ self.name = "Ditto" }
                    
                    Pokedex.shared.addPokemon(PokemonNovo: Pokemon(aName: self.name.capitalized, aImage: self.name.lowercased(), aType: self.tipo, aBaseHP: self.baseHp, aBaseAttack: self.baseAttack, aBaseDefense: self.baseDefense))
                    
                    appState.rootViewId = UUID()
                })
            }
        }
    }
}


struct BattleView: View { //View da Battle
    var myPokeId:UUID
    
    @State var searchText = ""
    @State var pokedexArView:[Pokemon] = Pokedex.shared.getArray()
    
    @State var myPokemon:Pokemon = Pokemon(aName: "", aImage: "", aType: "", aBaseHP: 1, aBaseAttack: 1, aBaseDefense: 1)
    @State var advPokemon:Pokemon = Pokemon(aName: "", aImage: "", aType: "", aBaseHP: 1, aBaseAttack: 1, aBaseDefense: 1)
    
    @EnvironmentObject var appState: AppState
    
    @State var idAdv:UUID = UUID()
    @State var myPokeHp = 0
    @State var advPokeHp = 0
    @State var msgJogo = ""
    @State var buttonLocked = false
    
    @State var movesAll:[String:Int] = ["Absorb":20,"Bubble":40,"Confusion":50,"Karate Chop":50,"Peck":35,"Thunderbolt":90,"Twineedle":25,"Ember":40] //Definiçao dos Moves disponives
    @State var myPokeMoves = [String:Int]()
    @State var advPokeMoves = [String:Int]()
    @State var myPokeMove1 = ""
    @State var myPokeMove2 = ""
    @State var advPokeMove1 = ""
    @State var advPokeMove2 = ""
                      
    var body: some View{
        ZStack{
            Color.green
                .ignoresSafeArea()
            
        VStack{
            HStack{ //Parte Gráfica Pokemon Adv
                Spacer()
                Text("Nível: \(advPokemon.level)     Vida: \(advPokeHp)   |   \(advPokemon.name)")
                    .fontWeight(.bold)
                
                Image(uiImage: advPokemon.image)
                    .resizable()
                    .scaledToFill()
                    .frame(width: 100, height: 100, alignment: .trailing)
                
            }
            
            HStack{ //Parte Gráfica do nosso Pokemon
                Image(uiImage: myPokemon.image)
                    .resizable()
                    .scaledToFill()
                    .frame(width: 100, height: 100)
                Text("\(myPokemon.name)   |   Vida: \(myPokeHp)    Nível: \(myPokemon.level)")
                    .fontWeight(.bold)
                Spacer()
            }
            
            HStack{
                VStack{//Parte Gráfica dos Moves e Mensagens de Jogo
                    Text("  \(msgJogo)")
                        .foregroundColor(.white)
                        .font(.system(size: 16))
                        .fontWeight(.bold)
                    
                    
                    HStack{
                        
                        Spacer()
                        
                        Button("\(myPokeMove1)") {
                            buttonLocked = true
                            
                            for (key, value) in myPokeMoves{ //Caso seja selecionado o Move 1 é efetuado o calculo do Dano e a sua efetividade
                                if key == myPokeMove1{
                                    
                                    var dmgFinal = (2 * myPokemon.level / 5 + 2) * value * myPokemon.attack / advPokemon.defense / 50 + 2
                                    
                                    if myPokemon.isStrongAg(aTypeAdvPoke: advPokemon.type){
                                        dmgFinal = dmgFinal * 2
                                    }
                                    if myPokemon.isWeakAg(aTypeAdvPoke: advPokemon.type){
                                        dmgFinal = dmgFinal / 2
                                    }
                                    
                                    advPokeHp -= dmgFinal
                                    msgJogo = " \(myPokemon.name) tirou \(dmgFinal) de dano com \(key)!"
                                }
                            }
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: { //Delay na atualização da Mensagem de Jogo
                            if advPokeHp < 1{ //Caso o Pokemon tenha perdido a vida toda entra aqui
                                myPokemon.afterBattle(aIsWinner: true)
                                msgJogo = "Ganhaste! O teu Pokemon tem agora \(myPokemon.xpCurrent) XP"
                                History.shared.addBattle(BattleNova: Battle(aPokemon1Name: myPokemon.name, aPokemon2Name: advPokemon.name, aWinner: 1))
                                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: { appState.rootViewId = UUID() }) //Delay no termino da Batalha
                            }
                            else{
                                self.turnoAdv()
                            }})
                        }
                        .disabled(buttonLocked)
                        .frame(width: 100, height: 50, alignment: .center)
                        .background(Color.gray)
                        .foregroundColor(.white)
                        .padding(5)
                        .overlay(RoundedRectangle(cornerRadius: 16) .strokeBorder(lineWidth: 5))
                        
                        Spacer()
                        
                        Button("\(myPokeMove2)") { //Caso seja selecionado o Move 2 é efetuado o calculo do Dano e a sua efetividade
                            buttonLocked = true
                            
                            for (key, value) in myPokeMoves{
                                if key == myPokeMove2{
                                    
                                    var dmgFinal = (2 * myPokemon.level / 5 + 2) * value * myPokemon.attack / advPokemon.defense / 50 + 2
                                    
                                    if myPokemon.isStrongAg(aTypeAdvPoke: advPokemon.type){
                                        dmgFinal = dmgFinal * 2
                                    }
                                    if myPokemon.isWeakAg(aTypeAdvPoke: advPokemon.type){
                                        dmgFinal = dmgFinal / 2
                                    }
                                    
                                    advPokeHp -= dmgFinal
                                    msgJogo = " \(myPokemon.name) tirou \(dmgFinal) de dano com \(key)!"
                                }
                            }
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
                            if advPokeHp < 1{//Caso o Pokemon tenha perdido a vida toda entra aqui
                                myPokemon.afterBattle(aIsWinner: true)
                                msgJogo = "Ganhaste! O teu Pokemon tem agora \(myPokemon.xpCurrent) XP"
                                History.shared.addBattle(BattleNova: Battle(aPokemon1Name: myPokemon.name, aPokemon2Name: advPokemon.name, aWinner: 1))
                                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: { appState.rootViewId = UUID() })
                            }
                            else{
                                self.turnoAdv()
                            }})
                        }
                        .disabled(buttonLocked)
                        .frame(width: 100, height: 50, alignment: .center)
                        .background(Color.gray)
                        .foregroundColor(.white)
                        .padding(5)
                        .overlay(RoundedRectangle(cornerRadius: 16) .strokeBorder(lineWidth: 5))
                        
                        Spacer()
                        
                        Button("Fugir!") { //Botão de Fuga caso calha um num acima do 3 entre 0 e 10 consegue fugir
                            buttonLocked = true
                            
                            let fuga = Int.random(in: 0..<10)
                            
                            if fuga > 3{
                                
                                msgJogo = "Fuga com sucesso!"
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: { appState.rootViewId = UUID() })
                            }
                            else{
                                msgJogo = "Fuga não foi bem sucessida..."
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: { self.turnoAdv() })
                            }
                        }
                        .disabled(buttonLocked)
                        .frame(width: 100, height: 50, alignment: .center)
                        .background(Color.gray)
                        .foregroundColor(.white)
                        .padding(5)
                        .overlay(RoundedRectangle(cornerRadius: 16) .strokeBorder(lineWidth: 5))
                        
                        Spacer()
                    }
                    .frame(height: 100, alignment: .center)
                    .background(Color.white)
                    .padding(10)
                    .overlay(RoundedRectangle(cornerRadius: 16) .strokeBorder(lineWidth: 10))
                    
                    }
                }
                        
            }
            .background(Color.green)
            .navigationBarBackButtonHidden(true)
            .onAppear{
                for entry in pokedexArView{ //Inicializaçao Local do nosso Pokemon
                    if entry.id == self.myPokeId {
                        myPokemon = entry
                    }
                }
                
                var repetido = true
                
                repeat{ //Gerar Adv Random
                    let geraAdvRandom = Int.random(in: 0..<pokedexArView.count)
                            
                    for (index, novo) in pokedexArView.enumerated(){
                        if index == geraAdvRandom && novo.isActive == true && novo.id != myPokeId{
                            advPokemon = novo
                            repetido = false
                        }
                    }
                } while repetido
                
                myPokeHp = myPokemon.hp
                advPokeHp = advPokemon.hp
                
                var repeteMoves = 0
                var nrAntes = -1
                var numRandom = 0
                
                repeat{ //Gerar Moves Random para nosso Pokemon
                    numRandom = Int.random(in: 0..<movesAll.count)

                    if nrAntes != numRandom{
                        for (index, (key,value)) in movesAll.enumerated(){
                            if index == numRandom{
                                myPokeMoves[key] = value
                                
                                if repeteMoves == 0{
                                    myPokeMove1 = key
                                } else{
                                    myPokeMove2 = key
                                }
                                
                            }
                        }
                        repeteMoves += 1
                    }
                    nrAntes = numRandom
                }while repeteMoves != 2
                
                repeteMoves = 0
                nrAntes = -1
                
                repeat{ //Gerar Moves Random para Pokemon Adv
                    numRandom = Int.random(in: 0..<movesAll.count)

                    if nrAntes != numRandom{
                        for (index, (key,value)) in movesAll.enumerated(){
                            if index == numRandom{
                                advPokeMoves[key] = value
                                
                                if repeteMoves == 0{
                                    advPokeMove1 = key
                                } else{
                                    advPokeMove2 = key
                                }
                            }
                        }
                        repeteMoves += 1
                    }
                    nrAntes = numRandom
                }while repeteMoves != 2
                
            }
        }
    }
        
    func turnoAdv(){ //Função para simular Turno Adv
            let mvRandom = Int.random(in: 0..<advPokeMoves.count)
                
            for (index, (key, value)) in advPokeMoves.enumerated(){
                if index == mvRandom{
                        
                    var dmgFinal = (2 * myPokemon.level / 5 + 2) * value * myPokemon.attack / advPokemon.defense / 50 + 2
                        
                    if advPokemon.isStrongAg(aTypeAdvPoke: myPokemon.type){
                        dmgFinal = dmgFinal * 2
                    }
                    if advPokemon.isWeakAg(aTypeAdvPoke: myPokemon.type){
                        dmgFinal = dmgFinal / 2
                    }
                        
                    myPokeHp -= dmgFinal
                        
                    msgJogo = "\(advPokemon.name) tirou \(dmgFinal) de dano com \(key)!"

                }
            }
                
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
                if myPokeHp < 1{
                    myPokemon.afterBattle(aIsWinner: false)
                    
                    msgJogo = "Perdeste! O teu Pokemon tem agora \(myPokemon.xpCurrent) XP"
                    
                    History.shared.addBattle(BattleNova: Battle(aPokemon1Name: myPokemon.name, aPokemon2Name: advPokemon.name, aWinner: 2))

                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: { appState.rootViewId = UUID() })
                }
                else{
                    buttonLocked = false
                }
            })
    }
    
}

/*
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
        }
    }
}*/
