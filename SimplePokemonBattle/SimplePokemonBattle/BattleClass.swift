//
//  BattleClass.swift
//  SimplePokemonBattle
//

import Foundation

public class Battle: Identifiable{
    public var id = UUID()
    var pokemon1Name:String
    var pokemon2Name:String
    var winner:Int
    
    init(aPokemon1Name:String, aPokemon2Name:String, aWinner:Int){
        self.pokemon1Name = aPokemon1Name
        self.pokemon2Name = aPokemon2Name
        self.winner = aWinner
    }
    
}


public class History{ // Adaptação para ter um Array Global
    static let shared = History()
    var historyAr = [Battle]()
    
    func getArray()->[Battle]{ //Get do conteudo do Array
        return historyAr
    }
    
    func addBattle(BattleNova: Battle){ //Funçao para adicionar novo historico
        historyAr.append(BattleNova)
    }
}
