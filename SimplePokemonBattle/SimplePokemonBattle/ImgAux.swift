//
//  ImgAux.swift
//  SimplePokemonBattle
//

import Foundation
import UIKit

extension UIImage{
    static var defImg:UIImage{
        UIImage(named: "noImg")!
    }
}

final class AppState : ObservableObject { //Código para voltar Root View
        @Published var rootViewId = UUID()
}
