//
//  SimplePokemonBattleApp.swift
//  SimplePokemonBattle
//

import SwiftUI
@main
struct SimplePokemonBattleApp: App {
    @ObservedObject var appState = AppState()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(appState)
        }
    }
}
